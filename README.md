#  Test deployment

@jorgemustaine 2020

![DevOps ico](static/images/devops_icon_00.png)

Is an exercise to implement a line of software production and automatize it's
flow with a set of tools and DevOps methodologies.

## upload a docker image to gitlab registry repository

~~~
docker login registry.gitlab.com
docker build -f ./docker/Dockerfile -t registry.gitlab.com/gitops-demo1/test_deployment .
docker push registry.gitlab.com/gitops-demo1/test_deployment
~~~

## Interesting links:

* [tuto about initial implementation](https://medium.com/urbaner/despliegue-con-gitlab-ci-cd-y-docker-swarm-e5869d4d20f)
* [gitlab-ci ENV vars](https://docs.gitlab.com/ee/ci/variables/)

